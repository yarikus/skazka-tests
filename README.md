# Tests

## Зависимости

* https://nodejs.org/
* https://www.typescriptlang.org/
* https://mochajs.org/

## Как установить

1. Установить зависимости
2. Склонировать репозиторий
2. Из директории проекта выполнить команду `npm install`

## Как запускать

```
$ npm run test
```