#!/usr/bin/env node

import {argv} from "yargs";
import {Builder, By, Key, until} from 'selenium-webdriver';

const mySexButtonSelector = By.css('.step_chatbox > div > div:nth-child(1) > div:nth-child(1) > div > button:nth-child(3)');
const maleButtonSelector = By.css('.step_chatbox > div > div:nth-child(1) > div:nth-child(2) > div > button:nth-child(2)');
const ageButtonSelector = By.css('.step_chatbox > div > div.row.row-search > div:nth-child(1) > button:nth-child(4)');
const searchButton = By.css('#searchCompanyBtn');
const chatStatus = By.css('.window_chat_statuss');
const loaderSelector = By.css('.load_init_step');
const input = By.css('.emojionearea-editor');

(async function example() {
    let driver = await new Builder().forBrowser('chrome').build();
    let count = 0;

    async function waitMessage() {
        let arr = await driver.findElements(By.css('.window_chat_dialog_block_nekto'));
        if (arr.length === 0) {
            await driver.wait(until.elementLocated(By.css('.window_chat_dialog_block_nekto'), 10000));
            let text = await driver.findElement(By.css('.window_chat_dialog_block_nekto .window_chat_dialog_text')).getText();

            await driver.sleep(3000);

            console.log(bot.getAnswer(text));

            await driver.findElement(input).click();
            await driver.findElement(input).sendKeys(bot.getAnswer(text), Key.RETURN);
            count = 1;
        } else if (count < arr.length) {
            count = arr.length;

            let text = await arr[arr.length - 1].findElement(By.css('.window_chat_dialog_text')).getText();

            await driver.sleep(3000);

            console.log(bot.getAnswer(text));

            await driver.findElement(input).click();
            await driver.findElement(input).sendKeys(bot.getAnswer(text), Key.RETURN);
        }

        waitMessage();
    }

    const bot: ChatBot = new ChatBot(Mood.happy, false);

    try {
        await driver.get('https://nekto.me/chat/');
        await driver.wait(until.elementIsNotVisible(driver.findElement(loaderSelector)), 10000);
        await driver.findElement(mySexButtonSelector).click();
        await driver.findElement(maleButtonSelector).click();
        await driver.findElement(ageButtonSelector).click();
        await driver.findElement(searchButton).click();

        //await driver.wait(until.elementLocated(chatStatus, 10000));

        await waitMessage();

        //const arr = await driver.findElements();
        //console.log(arr.length);

        //await driver.findElement(input).sendKeys(bot.getGreeting(), Key.RETURN);

        // await driver.sleep(10000);
        //
        // await driver.findElement(input).sendKeys('ты откуда?', Key.RETURN);

        //await getLastMessage();

        //await driver.wait(until.titleIs('webdriver - Google Search'), 1000);
    } finally {
        await driver.quit();
    }
})();