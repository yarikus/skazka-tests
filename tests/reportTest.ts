//import * as assert from 'assert';
import { expect } from 'chai';
import {Builder, By, Key, until} from 'selenium-webdriver';

describe('Reports', () => {
    
    const domain = 'http://test.skazkasoft.com/';
    let driver = new Builder().forBrowser('chrome').build();

    after(() => {
        driver.quit();
    });

    it("проверяет залогинивание", async () => {
        await driver.get(domain);

        const username = await driver.findElement(By.id("loginform-username"));

        await username.click();
        await username.sendKeys('qa');

        const password = await driver.findElement(By.id("loginform-password"));

        await password.sendKeys('1234', Key.ENTER);

        await driver.wait(until.titleIs('Все рапорта'));
    });

    it('проверяет забой', async () => {
            await driver.get(domain + '/reports/index');

            const reports = await driver.findElements(By.css('a[href^="/reports/view?id="]'));
            const hrefs = await Promise.all(reports.map(item => item.getAttribute('href')));

            for (let [index, href] of hrefs.entries()) {

                await driver.get(href);
                await driver.wait(until.titleIs('Суточный рапорт'));

                const targetValue: string = await driver.findElement(By.xpath("//td[contains(text(), 'Забой на 24:00')]/following-sibling::td[1]")).getText();
                const sourceValue: string = await driver.findElement(By.xpath("//td[contains(text(), 'Примечания по работам')]//preceding::td[3]")).getText();

                if (Number.parseFloat(targetValue) !== Number.parseFloat(sourceValue)) {
                    throw Error("Не совпадают значения забоя в строке " + index);
                }

            }
    }).timeout(15 * 1000 * 60);

});